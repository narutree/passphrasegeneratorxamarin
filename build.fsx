// include Fake lib
#r @"packages/FAKE/tools/FakeLib.dll"
open Fake

// Default target
Target "Default" (fun _ ->
    trace "Hello World from FAKE"
)
Target "xkcdpass.dll" (fun _ ->
    ["xkcdpass.fs"; ]
    |> FscHelper.compile [
        FscHelper.Out "bin/xkcdpass.dll"
        FscHelper.Target FscHelper.TargetType.Library
    ]
    |> function 0 -> () | c -> failwithf "F# compiler return code: %i" c
)
Target "main.exe" (fun _ ->
    ["main.fs"]
    |> FscHelper.compile [
        FscHelper.Out "bin/main.exe"
        FscHelper.References ["bin/xkcdpass.dll"; ]
    ]
    |> function 0 -> () | c -> failwithf "F# compiler return code: %i" c
)
// dependencies
"xkcdpass.dll"
  ==> "main.exe"
  ==> "Default"
// start build
RunTargetOrDefault "Default"
