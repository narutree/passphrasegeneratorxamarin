﻿module Xkcdpass
open System
open System.Security.Cryptography

//cryto strong random number < range up to 65kish
let rec crng (maxValue:uint16) : uint16 = 
    let fullRange = UInt16.MaxValue / maxValue
    let rng16bit = 
        use rngCsp = new RNGCryptoServiceProvider()
        let randomNumber : byte[] = Array.zeroCreate 2
        rngCsp.GetBytes(randomNumber);
        BitConverter.ToUInt16(randomNumber, 0)
    let m = rng16bit
    //< NOT <= because that'd be one too many 0's
    match m < (fullRange * maxValue) with 
        | true -> m % maxValue
        | _ -> crng maxValue

let generatePassphrase (wordList:Collections.seq<string>) numWords = 
    let words = [1..numWords] |> Seq.map (fun i ->
        let length = wordList |> Seq.length
        let index = crng(uint16 length)
        let word = wordList |> Seq.nth (int index)
        word)


    let s = Seq.fold (fun acc elem -> acc + elem + " ") "" words
    s
