﻿namespace xkcdpass

open System

open Android.App
open Android.Content
open Android.OS
open Android.Runtime
open Android.Views
open Android.Widget
open System.IO

open Xkcdpass

[<Activity (Label = "xkcdpass", MainLauncher = true)>]
type MainActivity () =
    inherit Activity ()

    override this.OnCreate (bundle) =

        base.OnCreate (bundle)

        // Set our view from the "main" layout resource
        this.SetContentView (Resource_Layout.Main)

        let sNumWords = this.FindViewById<NumberPicker>(Resource_Id.numWords)
        sNumWords.MaxValue <- 15
        sNumWords.MinValue <- 3
        sNumWords.Value <- 5

        let generateButton = this.FindViewById<Button>(Resource_Id.generateButton)
        let tPassphrase = this.FindViewById<TextView>(Resource_Id.passphrase)

        let wordFile = this.Assets.Open ("3esl.txt")

        let tWords = seq {
            use sr = new StreamReader (wordFile)
            while not sr.EndOfStream do
                yield sr.ReadLine()
        }
        let words = Seq.toList tWords

        // Get our button from the layout resource, and attach an event to it
        //let button = this.FindViewById<Button>(Resource_Id.myButton)
        generateButton.Click.Add (fun args -> 
            let numWords = sNumWords.Value
            let p = Xkcdpass.generatePassphrase words numWords
            tPassphrase.Text <- sprintf "%s" p
        )

