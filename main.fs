open System
open Xkcdpass

[<EntryPoint>]
let main argv = 
    let passphrase = Xkcdpass.generatePassphrase ["a"; "b"; "c"] 2
    printfn "%A" passphrase
    Console.ReadLine() |> ignore
    0
